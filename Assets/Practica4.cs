﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Practica4 : MonoBehaviour {

    

	// Use this for initialization
	void Start () {
        string stringEjemplo = "String de ejemplo";

        string serialization = StringSerializationAPI.Serialize(stringEjemplo.GetType(), stringEjemplo);

        Debug.Log("Serialized: " + serialization);

        string stringDeserialized = (string)StringSerializationAPI.Deserialize(typeof(string), serialization);

        Debug.Log("Deserialized: " + stringDeserialized);

        string serialization2 = StringSerializationAPI.serializeStringToJson("El sergi es manco");

        Debug.Log("Serialized string: " + serialization2);

        string deserialization2 = StringSerializationAPI.deserializeJsonToString(serialization2);

        Debug.Log("Deserialized string: " + deserialization2);


        List<Nave> listN = new List<Nave>();

        for (int i = 0; i < 3; i++)
        {
            Nave n = new Nave();
            n.posX = Random.Range(-1000, 1000);
            n.posY = Random.Range(-1000, 1000);
            n.vX = Random.Range(-50, 50);
            n.vY = Random.Range(-50, 50);
            n.angle = Random.Range(0, 360);
            
            Debug.Log("Creada nave numero " + (i + 1));

            listN.Add(n);
        }


        string json = StringSerializationAPI.Serialize(listN.GetType(), listN);

        Debug.Log("Serialized ships: " + json);

        List<Nave> listNdesserial = (List<Nave>)StringSerializationAPI.Deserialize(typeof(List<Nave>), json);

        foreach (Nave n in listNdesserial)
        {
            Debug.Log(n.ToString());
        }


	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
