﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System.IO;

public class practica3_JSON : MonoBehaviour {

	void Start () {

        string filepath = Application.persistentDataPath + "/Highscores.json";
        if (!File.Exists(filepath)) 
        {
            File.Copy(Application.streamingAssetsPath + "/Highscores.json", filepath);
        }

        StreamReader fichero = new StreamReader(filepath);
        string highscores = fichero.ReadToEnd();
        //JSONNode.Parse(filepath);
        //Debug.Log(highscores);
        fichero.Close();
	}
	
}
