﻿using SQLite4Unity3d;

public class usersAlternative {

	[PrimaryKey, AutoIncrement]
	public int id { get; set; }
	public string name { get; set; }
	public int highscore { get; set; }

	public override string ToString ()
	{
		return string.Format ("[Person: id={0}, name={1}, highscore={2}]", id, name, highscore);
	}
}
