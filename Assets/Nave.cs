﻿using UnityEngine;

public class Nave : Object{

    public float posX { get; set; }
    public float posY { get; set; }
    public float vX { get; set; }
    public float vY { get; set; }
    public float angle { get; set; }

    public override string ToString()
    {
        return string.Format("[Nave] ({0},{1}), v({2},{3}), a({4})",posX,posY,vX,vY,angle);
    }

    
}
